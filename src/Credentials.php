<?php
/**
 * Credentials.php file.
 *
 * @author Dirk Adler <adler@spacedealer.de>
 * @link http://www.spacedealer.de
 * @copyright Copyright &copy; 2014 spacedealer GmbH
 */


namespace spacedealer\cloudcontrol;


use yii\helpers\Json;

/**
 * Class Credentials
 *
 * @package spacedealer\cloudcontrol
 */
class Credentials
{
    /**
     * @var array
     */
    static private $_credentials;

    /**
     * Loads credentials either via cloudcontrol json file or via local config php file and returns them as array.
     *
     * @param $configDir
     * @return array
     */
    static public function load($configDir)
    {
        // running on cloudcontrol?
        if (Cloudcontrol::isRunningInCloud()) {
            if (isset($_SERVER['CRED_FILE'])) {
                // running on box
                // Parse the json file with ADDONS credentials
                $string = file_get_contents($_SERVER['CRED_FILE'], false);
                if ($string == false) {
                    die('FATAL: Could not read credentials file (cloudcontrol).');
                }
            } else {
                // during deployment process > before building image
                // deployment tasks based on yii console app should not depend on credentials
                $string = false;
            }
            self::$_credentials = Json::decode($string);
        } else if (file_exists($configDir . '/credentials-local.php')) {
            self::$_credentials = require($configDir . '/credentials-local.php');
        } else {
            die('FATAL: Could not read credentials file (local).');
        }

        return self::$_credentials;
    }

    /**
     * Returns credentials as array.
     * Note: please call init before to initialize credentials array
     *
     * @return array
     */
    static public function get()
    {
        if (!isset(self::$_credentials)) {
            die('FATAL: Credentials::init must be called first.');
        }

        return self::$_credentials;
    }
}