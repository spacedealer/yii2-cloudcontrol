<?php
/**
 * CloudcontrolController.php file.
 *
 * @author Dirk Adler <adler@spacedealer.de>
 * @link http://www.spacedealer.de
 * @copyright Copyright &copy; 2014 spacedealer GmbH
 */


namespace spacedealer\cloudcontrol\console\controllers;


use spacedealer\cloudcontrol\Cloudcontrol;
use spacedealer\cloudcontrol\Credentials;
use yii\console\Controller;
use yii\di\Instance;

/**
 * Class CloudcontrolController
 *
 * @package spacedealer\cloudcontrol\console\controllers
 */
class CloudcontrolController extends Controller
{
    /**
     * @var string|Cloudcontrol Cloudcontrol component ID. you can change this on cli to use a different configuration setting
     */
    public $cloudcontrol = 'cloudcontrol';

    /**
     * @inheritdoc
     */
    public $defaultAction = 'info';

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        // init cloudcontrol component
        if (is_string($this->cloudcontrol)) {
            $this->cloudcontrol = Instance::ensure($this->cloudcontrol, Cloudcontrol::className());
        }

        return parent::beforeAction($action);
    }

    /**
     * Prints summary about current configuration settings
     */
    public function actionInfo()
    {
        echo "\nCurrent configuration:\n\n";
        echo "appName:\n";
        echo "   " . $this->getAppName() . "\n\n";
        echo "deploymentName:\n";
        echo "   " . $this->getDeploymentName() . "\n\n";
        echo "addons:\n";
        foreach ($this->getAddons() as $addon) {
            echo "   " . $addon . "\n";
        }
        echo "\n";
    }

    /**
     * Update config settings in config.addon based on local config vars array.
     * Current active git branch will be used for cloudcontrol deployment settings
     */
    public function actionUpdateConfig()
    {
        $config = Credentials::get();

        if (!isset($config['CONFIG']['CONFIG_VARS'])) {
            echo "\nNo credentials found. Please set '['CONFIG']['CONFIG_VARS'] keys in credentials config file.\n";
        }

        // first remove current config settings
        $this->runCctrlapp("addon.remove config.free");

        // second add new settings
        $action = "addon.add config.free";
        foreach ($config['CONFIG']['CONFIG_VARS'] as $k => $v) {
            $action .= ' --' . $k . "='$v'"; // TODO: escape value
        }
        $this->runCctrlapp($action);
    }

    /**
     * Push and deploy current git branch to cloudcontrol
     */
    public function actionUpdate()
    {
        // first push
        $this->actionPush();

        // second deploy
        $this->actionDeploy();
    }

    /**
     * Init configured addons in current deployment / git branch in cloudcontrol
     * TODO: list addons first
     */
    public function actionInitAddons()
    {
        $addons = $this->getAddons();
        foreach ($addons as $addon) {
            $this->runCctrlapp("addon.add $addon");
        }
    }


    /**
     * Push current git branch to cloudcontrol
     * Update iron worker
     * Update cloudcontrol config
     * Deploy current git branch to cloudcontrol
     * TODO: add deployment to maintenance mode
     */
    public function actionRelease()
    {
        // TODO: execute pre push commands

        // first push
        $this->runCctrlapp('push');

        // TODO: execute pre config commands

        // update config before deploying new image
        $this->actionUpdateConfig();

        // TODO: execute pre deploy / post config commands

        // finally deploy
        $this->runCctrlapp('deploy');

        // TODO: execute post push commands

        \Yii::info('Released.', 'spacedealer/yii2-cloudcontrol');
    }

    /**
     * Deploy current git branch on cloudcontrol
     */
    public function actionDeploy()
    {
        $this->runCctrlapp('deploy');
    }

    /**
     * Push current git branch to cloudcontrol
     */
    public function actionPush()
    {
        $this->runCctrlapp('push');
    }

    /**
     * List or add new worker
     *
     * @param $name
     */
    public function actionWorker($name = null)
    {
        if (!$name) {
            $this->runCctrlapp('worker');
        } else {
            $this->runCctrlapp('worker.add ' . $name);
        }
    }

    /**
     * Restart all or single worker
     *
     * @param null $id
     */
    public function actionWorkerRestart($id = null)
    {
        if (!$id) {
            $this->runCctrlapp('worker.restart --all');
        } else {
            $this->runCctrlapp('worker.restart ' . $id);
        }
    }

    /**
     * Remove single worker
     */
    public function actionWorkerRemove($id)
    {
        $this->runCctrlapp('worker.remove ' . $id);
    }

    /**
     * Execute any given cctrlapp action on current active deployment.
     *
     * @param $action
     */
    public function actionApp($action)
    {
        $args = @$_SERVER['argv'];
        $action = array_slice($args, 2);
        $action = implode(' ', $action);
        $this->runCctrlapp($action);
    }

    /**
     * @return string
     */
    protected function getDeploymentName()
    {
        return $this->cloudcontrol->getDeploymentName();
    }

    /**
     * @return string
     */
    protected function getAppName()
    {
        return $this->cloudcontrol->appName;
    }

    protected function getAddons()
    {
        return $this->cloudcontrol->addons;
    }

    protected function runCctrlapp($action)
    {
        $cmd = $this->cloudcontrol->getCmd($action);
        echo "Executing: $cmd\n";
        $this->cloudcontrol->runCctrlapp($action);
    }
}
