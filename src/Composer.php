<?php
/**
 * Composer.php file.
 *
 * @author Dirk Adler <adler@spacedealer.de>
 * @link http://www.spacedealer.de
 * @copyright Copyright &copy; 2014 spacedealer GmbH
 */


namespace spacedealer\cloudcontrol;

/**
 * Class Composer
 *
 * @package spacedealer\cloudcontrol
 */
class Composer
{
    /**
     * Init environment based on current deployment name
     * It depends on https://github.com/yiisoft/yii2-app-advanced
     */
    public static function postUpdate()
    {
        $deploymentName = Cloudcontrol::findDeploymentName();

        echo "Found environment: $deploymentName.\n\n";

        // activate & copy config env files
        passthru('./init --env=' . $deploymentName . ' --overwrite=y');

        echo "Activated environment settings.\n";
    }
} 