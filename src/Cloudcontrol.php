<?php
/**
 * Cloudcontrol.php file.
 *
 * @author Dirk Adler <adler@spacedealer.de>
 * @link http://www.spacedealer.de
 * @copyright Copyright &copy; 2014 spacedealer GmbH
 */

namespace spacedealer\cloudcontrol;

use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class Cloudcontrol
 *
 * @package spacedealer\cloudcontrol
 */
class Cloudcontrol extends Component
{
    /**
     * @var string Path to the git executable
     */
    public $gitPath = '/usr/bin/git';

    /**
     * @var string Path to the cctrlapp executable.
     * @see https://github.com/cloudControl/cctrl
     */
    public $cctrlappPath = '/usr/bin/cctrlapp';


    /**
     * @var string
     */
    public $appName;

    /**
     * structure:
     * ['alias.wildcard', 'mysqld.micro']
     *
     * @var array Cloudcontrol addons that should be added on init
     */
    public $addons = [];

    // TODO: finish pre / post commands - alternativly: better use events for hooks?!
    public $commands = [];

    /**
     * @var string
     */
    protected $deploymentName;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        // test app name
        if (!isset($this->appName)) {
            throw new InvalidConfigException('\spacedealer\cloudcontrol\Extension appName not set.');
        }

        // test git
        if (!is_executable($this->gitPath)) {
            throw new InvalidConfigException('\spacedealer\cloudcontrol\Extension gitPath not valid. Missing file or not exectubale.');
        }

        // test cctrlapp
        if (!is_executable($this->cctrlappPath)) {
            throw new InvalidConfigException('\spacedealer\cloudcontrol\Extension cctrlappPath not valid. Missing file or not exectubale.');
        }
    }

    /**
     * Execute cctrlapp action on current active deployment.
     *
     * @param $action
     */
    public function runCctrlapp($action)
    {
        $cmd = $this->getCmd($action);

        \Yii::info("Executing: $cmd", 'spacedealer/yii2-cloudcontrol');
        passthru($cmd);
    }

    public function getCmd($action)
    {
        $appName = $this->appName;
        $deploymentName = $this->getDeploymentName();

        return $this->cctrlappPath . " $appName/$deploymentName $action";
    }

    /**
     * Get current active git branch as deployment name.
     *
     * @return string
     */
    public function getDeploymentName()
    {
        if (!isset($this->deploymentName)) {
            $this->deploymentName = self::findDeploymentName($this->gitPath);
        }

        return $this->deploymentName;
    }

    /**
     * Finds current deployment name.
     *
     * When running locally git is used to extract current active branch as deployment name.
     * When running on cloudcontrol deployment name is extracted from $_ENV['DEP_NAME']
     *
     * @param string $gitPath
     * @return string
     */
    public static function findDeploymentName($gitPath = '/usr/bin/git')
    {
        if (self::isRunningInCloud()) {
            list($appName, $deploymentName) = explode('/', $_SERVER['DEP_NAME'], 2);

            return $deploymentName;

        } else if (is_executable($gitPath)) {
            @exec($gitPath . ' branch', $lines);
            $branch = '';
            foreach ($lines as $line) {
                if (strpos($line, '*') === 0) {
                    $branch = ltrim($line, '* ');
                    break;
                }
            }

            // master is mapped to default deployment
            if ($branch == 'master') {
                $branch = 'default';
            }

            return $branch;

        }

        return false;
    }

    /**
     * Whether current php app is running on cloudcontrol or not.
     *
     * TODO: works during deployment and via apache both cloudcontrol domains, but as worker and aliased?
     * TODO: test for _SERVER["DOMAIN"] - worker? aliased?
     *
     * @return bool
     */
    public static function isRunningInCloud()
    {
        return (
            isset($_SERVER['DEP_NAME'])
            && isset($_SERVER['DOMAIN'])
            && $_SERVER["DOMAIN"] == 'cloudcontrolled.com'
        );
    }
}
