Cloudcontrol Extension for Yii 2
================================

This extension provides cli tools and configuration handling to run Yii 2 applications on cloudcontrol.

Attention: Please do not use in production environments. It‘s WIP.


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist spacedealer/yii2-cloudcontrol "*"
```

or add

```
"spacedealer/yii2-cloudcontrol": "*"
```

to the require section of your `composer.json` file.


Usage
-----

TBD

Once the extension is installed, simply modify your application configuration as follows:

```php
//tbd.
```
